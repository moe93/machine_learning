# @formatter:off

from    argparse                    import  ArgumentParser              # Add input arguments to script
import  datetime                    as      dt                          # Generate formatted date and time
import  numpy                       as      np                          # Advanced maths package
import  pandas                      as      pd                          # Data frames
import  pandas_datareader           as      web                         # Query data from online sources
import  matplotlib.pyplot           as      plt                         # Plotting
import  tensorflow                                                      # Neural network main engine
import  joblib

## --------------------------------------------------------------------
## Setup various environment parameters
## --------------------------------------------------------------------

# Uncomment below to hide GPU from visible devices
tensorflow.config.set_visible_devices( [], 'GPU' )                      # Disable TF GPU acceleration

plt.style.use('fivethirtyeight')

## --------------------------------------------------------------------
## Define range and stock/crypto of interest
## --------------------------------------------------------------------
t0  = dt.datetime( 2017, 1, 1 )                                         # Start time
tf  = dt.datetime( 2022, 6, 1 )                                         # End time
ticker  = 'BTC-USD'


## --------------------------------------------------------------------
## Retrieve data
## --------------------------------------------------------------------
df  = web.DataReader( ticker, data_source='yahoo', start=t0, end=tf )   # Read range from Yahoo
df  = df.sort_values( 'Date' )                                          # Sort by date just in case
# ___START___:  ***NOTE: Need to change index in order to reformat the date to proper style
#                           v v v v v v v v v v v
df  = df.reset_index()                                                  # Changes index from Date to 0 ---> end
#                           ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^
# ___END___:    ***NOTE
df['Date'] = pd.to_datetime( df.Date, format='%Y-%m-%d' )               # Reformat date to YYYY-mm-dd as is the convention
df.index = df['Date']                                                   # Change index back to Date from 0 ---> end

## --------------------------------------------------------------------
## Plot to visualize data
## --------------------------------------------------------------------
# plt.figure( figsize=(16, 8), dpi=300 )
# plt.plot( df["Close"], label='Close Price history' )
# plt.show()

## --------------------------------------------------------------------
## Preprocess dataset
## --------------------------------------------------------------------
# Create an empty dataframe similar to df in shape, containing only 'Date' and 'Close' headers
dataset = pd.DataFrame( index=range(0, len(df)), columns=['Date', 'Close'] )    # New dataset

# Populate the new data set (Make exact copy of df entries)
for i in range(0, len(df)):
    dataset['Date'][i]  = df['Date'][i]
    dataset['Close'][i] = df['Close'][i]

dataset.index = dataset.Date                                        # Change index to Date from 0 ---> end
dataset.drop("Date", axis=1, inplace=True)                          # Drop/remove the Date header as we don't need it anymore

## --------------------------------------------------------------------
## Load saved LSTM model
## --------------------------------------------------------------------
# Load the compiled model
from    keras.models        import  load_model
regressor = load_model( f'{ticker}_LSTM_model.h5' )
print( regressor.summary() )

# Load scales
scaler = joblib.load( f'{ticker}_LSTM_model_scales.save' )

## --------------------------------------------------------------------
## Import data for prediction and forecasting
## --------------------------------------------------------------------

# Define constant parameters
SEQ_LEN = 60                                                        # Sequence length (number of days)
N_DAYS  = 1#0                                                         # Number of days to forecast into the future

tf  = dt.datetime( 2022, 5, 20 )                                    # End time
# delta = tf - dt.datetime.today()
# N_DAYS = delta.days                                                    # Number of days to forecast into the future

# Get the quote
data = web.DataReader( ticker, data_source='yahoo', start=t0, end=tf )
new_df = data.filter( ['Close'] )                                   # Create a new dataframe

## --------------------------------------------------------------------
## Forecast given data and compare to known truth to view model
##  accuracy and gain confidence it works, yay =D
## --------------------------------------------------------------------
confidence_data = new_df.values[0: , :]                             # Training data

for i in range( 0, 2):#N_DAYS ):
    confidence_scaled = scaler.transform( confidence_data[:,i].reshape(1967, 1) )
    #Create the x_test data sets
    x_test = []
    for j in range( SEQ_LEN, len(confidence_scaled) ):
        x_test.append( confidence_scaled[j-SEQ_LEN:j, 0] )

    # Convert into a NumPy array
    x_test = np.array(x_test).astype(np.float32)
    # Scale the data
    # x_scaled = scaler.transform( x_test[:,i].reshape((x_test[:,i].shape)[0], 1) )
    # reshape input to be [samples, time steps, features] which is required for LSTM
    # Confidence_test = np.reshape(x_scaled, (x_test.shape[0], x_test.shape[1], 1))
    Confidence_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))

    ## --------------------------------------------------------------------
    ## Predict future of known truth
    ## --------------------------------------------------------------------
    truth_predictions = regressor.predict( Confidence_test )                    # Predict
    truth_predictions = scaler.inverse_transform( truth_predictions )       # Undo 0 ---> 1 scaling
    print( truth_predictions )

    # Append predictions to array
    temp_array = np.append( (confidence_data[1:,i].reshape(confidence_data[1:,i].shape[0],1)), truth_predictions[-1:], axis=0 )     # Append predictions at end of column
    confidence_data = np.append( confidence_data, temp_array, axis=1 )    # Append new row with predictions row-wise


## --------------------------------------------------------------------
## Plot
## --------------------------------------------------------------------
# For when we don't know the truth
actual  = df[:]                                                         # Truth
pred    = new_df[SEQ_LEN:]                                              # Predicted data dataframe for plotting
pred.insert( len(pred.columns), 'Predictions', truth_predictions )      # Add the predictions to the validation data for plotting
pred = pred.drop( 'Close', axis=1 )                                     # Drop/remove the Close header as we don't need it anymore

# ___START___:  ***NOTE: Need to change index in order to reformat the date to proper style
#                           v v v v v v v v v v v
pred  = pred.reset_index()                                              # Changes index from Date to 0 ---> end
#                           ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ ^
# ___END___:    ***NOTE
for i in range( 1, N_DAYS + 1 ):                                        # N_DAYS inclusive loop (stops iterating at N_DAYS+1)
    pred.loc[ pred.shape[0] ] = [pred.Date[pred.shape[0]-1] + dt.timedelta(days=1), forecasting_data[-1,i]]   # Add the predictions one day (1 day) later for plotting

pred['Date'] = pd.to_datetime( pred.Date, format='%Y-%m-%d' )           # Reformat date to YYYY-mm-dd as is the convention
pred.index = pred['Date']                                               # Change index back to Date from 0 ---> end

# Get x-axis limits
xmin    = dt.datetime( 2021, 5, 25 )                                    # x-axis min limit
xmax    = pd.to_datetime( pred['Date'][-1] + dt.timedelta(days=15) )    # x-axis max limit (15 days after latest prediction)

# Simplify dataframe
pred = pred.drop( 'Date', axis=1 )                                      # Drop/remove the Date header as we don't need it anymore

# Create linestyle dictionary for more refined control over styles
from collections import OrderedDict
linestyles_dict = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     ('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     ('loosely dashdotted',  (0, (3, 10, 1, 10))),
     ('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])
# USAGE:
# plt.plot( x_data, y_data, linestyle=linestyles_dict['densely dotted'] )

# Plot
plt.figure( figsize=(10, 5) )
plt.ion()
plt.plot( actual['Close'], linestyle=linestyles_dict['solid'] )
plt.plot( pred['Predictions'] )
plt.title('Model')
plt.xlabel('Date', fontsize=8)
plt.ylabel('Close Price USD ($)', fontsize=12)
plt.legend(['Truth', 'Predictions'], loc='lower right')
plt.xlim( [xmin, xmax] )
plt.show()