#@formatter:off
"""
*
* Build an LSTM model to predict future prices of stocks/cryptos of interest
* More generally, it can predict and forecast any timeseries.
*
* VERSION: 1.1
*   - ADDED     : Fixed typos.
*   - ADDED   : More arguments for ArgParser()
*
* KNOWN ISSUES:
*   - FIXED     : Circumvented a bug that might have cropped up later when changing t_final.
*   - FIXED     : Data sorting by date was not working properly. Now fixed.
*
* AUTHOR                    :   Mohammad Odeh
* DATE                      :   Jun.  3rd, 2022 Year of Our Lord
* LAST CONTRIBUTION DATE    :   Jun. 12th, 2022 Year of Our Lord
*
* =====================================================================
* -----------------------------   USAGE   -----------------------------
* =====================================================================
*
* 1- Define the ticker of the stock/crypto of interest.
*
* 2- Define start and end time used for training and building of the LSTM model.
*
* 3- Define n_lookback and n_forecast:
*    a) n_lookback: number of entries used to create in the 'past' sequence of data.
*           i.e.   n_lookback = 10 ; use the past 10 entries to create n_forecast predictions.
*    b) n_forecast: number of entries to predict (forecast) into the future.
*           i.e.   n_forecast =  5 ; predict 5 entries into the future using n_lookback entries.
*
* NOTES:
*   - n_lookback (look back period) in the range of [2 - 5] provides the best
*       predictive results with the lowest MAPE, MSE, and MSLE errors.
*       SOURCE: https://www.sciencedirect.com/science/article/pii/S1877050920308851?ref=cra_js_challenge&fr=RR-1
*
*   - A dropout rate in the range of [0.2 - 0.4] is advised based on empirical findings.
*       SOURCE: https://arxiv.org/pdf/1810.08606.pdf
*
"""

from    argparse                    import  ArgumentParser              # Add input arguments to script
from    sklearn.preprocessing       import  MinMaxScaler                # Squeeze and map data to 0 <---> 1 range
from    keras.models                import  Sequential                  # Sequential estimator
from    keras.layers                import  LSTM, Dropout               # Neural network params
from    keras.layers                import  Dense, Activation           # ...
from    math                        import  floor                       # Round down numbers
import  datetime                    as      dt                          # Generate formatted date and time
import  numpy                       as      np                          # Advanced maths package
import  pandas                      as      pd                          # Data frames
import  pandas_datareader           as      web                         # Query data from online sources
import  matplotlib.pyplot           as      plt                         # Plotting
import  tensorflow                                                      # Neural network main engine
import  joblib                                                          # Used to save/load model scales

#%% ----------------- ___START___: Construct Arg Parser -----------------
ap = ArgumentParser()

# Developer mode, makes life easy for me
string = 'Enter developer mode. [Default: False]'
ap.add_argument( '--dev-mode'           ,
                 dest   = 'dev_mode'    ,
                 action = 'store_true'  , default=False     ,
                 help   = f'{string}'                       )

# Print out stuff to help debug
string = 'WARNING: Prints EVERYTHING!! [Default: False]'
ap.add_argument( '-v'   , '--verbose'   ,
                 dest   = 'verbose'     ,
                 action = 'store_true'  , default=False     ,
                 help   = f'{string}'                       )

# Save model
string = 'Save trained model. [Default: True]'
ap.add_argument( '--save-model'         ,
                 dest   = 'save_model'  ,
                 action = 'store_true'  , default=True      ,
                 help   = f'{string}'                       )

# Path to compiled LSTM model executable
string = 'Path to model.h5'
ap.add_argument( '--load-model'         , type = str        ,
                 dest   = 'load_model'  , default = 'foo'   ,
                 help   = f'{string}'                       )

args = ap.parse_args()
#   ----------------- ___ END ___: Construct Arg Parser -----------------


#%% -------------- ___START___: Define environment params. --------------
args.dev_mode = False                                                   # Enable Developer mode
args.save_model = False                                                 # Don't save generated model

tensorflow.config.set_visible_devices( [], 'GPU' )                      # Disable TF GPU acceleration [COMMENT LINE TO ENABLE]
plt.style.use( 'fivethirtyeight' )                                      # Change matplotlib plot style

# Directories
model_dir   = 'models'                                                  # Save/load model to/from here
figure_dir  = 'figs'                                                    # Save generated plots here

# LSTM data sequencing parameters
n_lookback  = 5                                                         # Length of input  sequences (lookback period)
n_forecast  = 3                                                         # Length of output sequences (forecast period)
n_features  = 1                                                         # Number of features ( 1 == uni-variate, >1 == multivariate)
scaler      = MinMaxScaler( feature_range = (0, 1) )                    # Range to scale the data to [min, max]

# CONSTANTS
PRCNT       = 0.80                                                      # Percentage of data used for training
BATCH_SIZE  = 32                                                        # Number of data points to use at once for training
N_EPOCHS    = 200                                                       # Number of 'iterations' for training
#   -------------- ___ END ___: Define environment params. --------------


#%% -------------- ___START___: Retrieve stock/crypto data --------------
ticker      = 'BTC-USD'                                                 # Stock/crypto ticker of interest
t_initial   = dt.datetime( 2017, 1, 1 )                                 # Start time
# t_final     = dt.datetime( 2022, 6, 1 )                                 # End time
t_final     = dt.datetime.now()                                         # End time
t_final     = t_final.replace(microsecond=0,second=0,minute=0,hour=0)   # Remove hours, minutes, seconds, and microseconds

df  = web.DataReader( ticker                    ,                       # Query data from source
                      data_source   = 'yahoo'   ,
                      start         = t_initial ,
                      end           = t_final )

df          = df.reset_index()                                          # Change index from [ 'Date' ] to 0 ---> end
df          = df.sort_values( 'Date' )                                  # Sort by date in case data is NOT sorted
df['Date']  = pd.to_datetime( df.Date, format='%Y-%m-%d' )              # Reformat date to YYYY-mm-dd in case it is NOT
# df.index    = df['Date']                                                # Change index to [ 'Date' ] from 0 ---> end

# If wanting to load a local data file, uncomment line below and comment out data query above
# df          = pd.read_csv( 'BTC-USD_2017-01-01_2022-05-01.csv' )
#   -------------- ___ END ___: Retrieve stock/crypto data --------------


#%% ------------------ ___START___: Preprocess dataset ------------------
dataset = pd.DataFrame( index   = range( 0, len(df) ),                  # Create empty dataframe similar to df in shape
                        columns = ['Date' , 'Close'] )                  # containing only ['Date', 'Close'] headers

for NDX in range( 0, len(df) ):                                         # Loop over df and dataset
    dataset.loc[ NDX, 'Date' ] = df.loc[ NDX, 'Date' ]                  #   Copy [ 'Date'  ] entries from df to dataset
    dataset.loc[ NDX, 'Close'] = df.loc[ NDX, 'Close']                  #   Copy [ 'Close' ] entries from df to dataset

dataset.index = dataset.Date                                            # Change index to Date from 0 ---> end
dataset = dataset.drop( 'Date', axis=1 )                                # Drop Date header as we don't need it anymore

SPLIT   = floor( PRCNT*len(dataset.values) )                            # %PRCNT of the data used for training
train_data = dataset.values[ :SPLIT , : ]                               # Training data
valid_data = dataset.values[ SPLIT: , : ]                               # Validation data
#   ------------------ ___ END ___: Preprocess dataset ------------------


#%% --------------- ___START___: Create training data set ---------------

# The process of building sequences works by creating a sequence of a
#  specified length at position 0. Then we shift one position to the
#  right (e.g. 1) and create another sequence. The process is
#  repeated until all possible positions are used.
#
# Create a training dataset containing the last {n_lookback}-day closing price
#  values we want to use to estimate {n_lookback ---> n_forecast}-day closing price values.
#
# So, the first column in dataset ‘x_train’ will contain values from a
#  dataset from index 0 to index {n_lookback - 1}, and the second column will
#  contain values from a dataset from index 1 to index {n_lookback} then index
#  2 to index {n_lookback + 1} and so on.
#
# The data set ‘y_train’ is the {n_lookback + n_forecast} value(s) and so on.
train_scaled = scaler.fit_transform( train_data )                       # Scale data to [0, 1] range
X_train, Y_train = [], []                                               # Create lists to store training data sequence

for i in range( n_lookback, len(train_data) - n_forecast + 1 ):         # Loop over test_scaled dataset
    X_train.append( train_scaled[i - n_lookback: i] )                   #   Append          input  data
    Y_train.append( train_scaled[i: i + n_forecast] )                   #   Append expected output data

X_train = np.array(X_train)                                             # Cast X_train list into numpy array
Y_train = np.array(Y_train)                                             # Cast Y_train list into numpy array

X_train = np.reshape( a          = X_train,                             # Reshape X_test into [samples, time steps, features]
                      newshape   = (X_train.shape[0], n_lookback, n_features) )
Y_train = np.reshape( a          = Y_train,                             # Reshape Y_test into [samples, time steps, features]
                      newshape   = (Y_train.shape[0], n_forecast, n_features) )

#   --------------- ___ END ___: Create training data set ---------------


#%% ----------------- ___START___: Create test data set -----------------
test_scaled = scaler.transform( valid_data )                            # Scale data to [0, 1] range
X_test, Y_test = [], []                                                 # Create lists to store test data sequence

for i in range( n_lookback, len(test_scaled) - n_forecast + 1 ):        # Loop over test_scaled dataset
    X_test.append( test_scaled[i - n_lookback: i] )                     #   Append          input  data
    Y_test.append( test_scaled[i: i + n_forecast] )                     #   Append expected output data

X_test = np.array( X_test ).astype( np.float32 )                        # Cast X_test list into numpy array
Y_test = np.array( Y_test ).astype( np.float32 )                        # Cast Y_test list into numpy array

X_test = np.reshape( a          = X_test,                               # Reshape X_test into [samples, time steps, features]
                     newshape   = (X_test.shape[0], n_lookback, n_features) )
Y_test = np.reshape( a          = Y_test,                               # Reshape Y_test into [samples, time steps, features]
                     newshape   = (Y_test.shape[0], n_forecast, n_features) )
#   ----------------- ___ END ___: Create test data set -----------------


#%% --------- ___START___: Build/Load LSTM sequential estimator ---------
model_id = f'{ticker}_{n_lookback}lookback_{n_forecast}forecast_LSTM_model'

if( args.dev_mode ):
    from    keras.models                import  load_model
    regressor = load_model( f'{model_dir}/{model_id}.h5' )              # Load pre-built LSTM model
    scaler = joblib.load( f'{model_dir}/{model_id}_scales.save')        # Load pre-built LSTM model scales

else:
    N_L  : int  = 4                                                     # Number of layers in neural network
    
    N_i  : int  = n_lookback ; N_o: int = n_forecast                    # Number of input neurons; Number of output neurons
    N_s  : int  = SPLIT - n_lookback - n_forecast + 1                   # Number of samples in training dataset
    
    alpha: float= 2                                                     # Arbitrary scaling factor usually [2, 10]
    # N_h  : int  = int( N_s / (alpha*(N_i+N_o)) )                        # Number of hidden layer neurons
    N_h  : int  = int( 2/3 * (N_i+N_o) )                                # Alternative rule of thumb computation
    
    init_run = True                                                     # Fag to initialize estimator build
    for i in range( 0, N_L ):                                           # Build neural network
        print( i )
        if( init_run and i == 0 ):                                      #   If initial run
            init_run = False                                            #       Toggle flag to OFF
            
            regressor = Sequential()                                    #       Initialize Sequential estimator object
            regressor.add( LSTM(units               = N_i,              #       Add input layer
                                return_sequences    = True,
                                input_shape         = (n_lookback, n_features)
                                ) )
            regressor.add( Dropout(0.20) )                              #       Use a dropout rate to combat over-fitting
        
        elif( init_run is False and i == 0 ):                           #   Error handling
            print( '[INFO] Failed to initialize build model.'           #       [INFO] ...
                   '\nCheck that init_run==True prior to build' )       #       [INFO] ...
            quit()                                                      #       QUIT PROGRAM
        
        elif( not init_run and i > 0 ):                                 #   If not init_run and no errors
            if( i < N_L-1 ):                                            #       If 1 < i < N_L
                regressor.add( LSTM(units            = N_h,             #           Add the i-th layer
                                    return_sequences = True) )          #           ...
                regressor.add( Dropout(0.25) )                          #           Add dropout rate
            
            else:                                                       #       If i == N_L
                regressor.add( LSTM(units = N_h) )                      #           Add the (N_L-1) layer
                regressor.add( Dropout(0.30) )                          #           Add dropout rate
                
                regressor.add( Dense(units = N_o) )                     #           Add the (N_L) layer; Output
    
    regressor.compile( optimizer    = 'adam',                           # Compile the model
                       loss         = 'mean_squared_error',             # ...
                       metrics      = ['msle', 'cosine_similarity'] )   # ...
    
    # Callback for validation accuracy maximization
    from keras.callbacks import  ReduceLROnPlateau, EarlyStopping       # Import callback classes
    
    lr_reduce = ReduceLROnPlateau(  monitor     = "val_loss",           # Callback to reduce learning rate based on metric
                                    factor      = 0.5       ,           # ...
                                    patience    = 15        ,           # ...
                                    verbose     = 1         ,           # ...
                                    mode        = "auto"    ,           # ...
                                    min_delta   = 0.001     ,           # ...
                                    min_lr      = 1e-4 )                # ...
    
    early_stop = EarlyStopping(     monitor     = "msle"    ,           # Callback to stop early based on metric
                                    min_delta   = 0.001     ,           # ...
                                    patience    = 25        ,           # ...
                                    verbose     = 1         ,           # ...
                                    mode        = "auto"    ,           # ...
                                    baseline    = None      ,           # ...
                                    restore_best_weights = False )      # ...
    
    history = regressor.fit( X_train, Y_train, verbose = 2,             # Train the model
                             validation_data = ( X_test, Y_test )   ,   # ...
                             callbacks  = [ lr_reduce, early_stop ] ,   # ...
                             epochs     = N_EPOCHS                  ,   # ...
                             batch_size = BATCH_SIZE )                  # ...

    print( regressor.summary() )                                        # Print summary of neural network
    
    if( args.save_model ):
        regressor.save( f'{model_dir}/{model_id}.h5' )                  # Save a copy of the regressor LSTM model
        joblib.dump(scaler, f'{model_dir}/{model_id}_scales.save')      # Save a copy of the regressor LSTM model scales
    else: pass
    # plt.figure( figsize=(10, 5) )
    # plt.plot( history.epoch, history.history['loss'] )
    
    # Evaluate the model after training
    mdl_results = regressor.evaluate( X_test, Y_test,                   # Evaluate model using test data
                                        verbose = True )                # ...
    print( f'[INFO] Test results'                                       # [INFO] Print evaluation results
           f'\n\t- Loss: {mdl_results[0]:34.4f}'                        # ...
           f'\n\t- Mean Square Logarithmic Error: {mdl_results[1]:9.4f}'# ...
           f'\n\t- Cosine Similarity: {mdl_results[2]:21.4f}' )         # ...
#   --------- ___ END ___: Build/Load LSTM sequential estimator ---------


#%% ----- ___START___: Predict n_forecast points from training data -----
X_1: list[float] = scaler.transform( train_data.reshape(-1, 1) )        # Transform and reshape train_data into column vector
X_1: np.ndarray  = np.array( X_1[-(n_lookback):] ).astype( np.float32 ) # Last available input sequence as a NumPy array
train_n_lookback = X_1.reshape( (1, n_lookback, n_features) )           # Reshape into [samples, time steps, features]

train_n_forecast = regressor.predict( train_n_lookback ).reshape(-1, 1) # Predict n_forecast and reshape
train_n_forecast = scaler.inverse_transform( train_n_forecast )         # Undo 0 ---> 1 scaling
#   ----- ___ END ___: Predict n_forecast points from training data -----


#%% --------- ___START___: Verify validation data by predicting ---------
X_2 = X_1                                                               # Initialize X_2 as a copy of X_1
arr = []                                                                # Initialize empty array
for i in range( 1, n_lookback ):                                        # Loop over
    arr.append( np.append( X_2[ -n_lookback+i: ], X_test.flatten()[ :i ] ) )   # Append n_lookback data points to the beginning of X_test

arr = np.array( arr ).astype( np.float32 )                              # Cast list into NumPy array
X_2 = np.append( np.transpose( X_2 ), arr, axis=0 )                     # Transpose s.t [samples, time steps] then append temp_arr below
X_2 = np.append( X_2, X_test[:, :, 0], axis=0 )                         # Append X_test below X_2
valid_n_lookback = X_2.reshape( (X_2.shape[0], n_lookback, n_features) )# Reshape into [samples, time steps, features]

valid_n_forecast = regressor.predict( valid_n_lookback )                # Predict
valid_n_forecast = scaler.inverse_transform( valid_n_forecast )         # Undo 0 ---> 1 scaling
#   --------- ___ END ___: Verify validation data by predicting ---------

#%% ------ ___START___: Predict n_forecast points from dataset end ------
X_3 = scaler.transform( dataset.values.reshape(-1, 1) )                 # Transform and reshape dataset
X_3 = np.array( X_3[ -n_lookback: ] ).astype( np.float32 )              # Last available input sequence
data_n_lookback = X_3.reshape( (1, n_lookback, n_features) )            # Reshape into [samples, time steps, features]

data_n_forecast = regressor.predict( data_n_lookback ).reshape( -1, 1 ) # Predict
data_n_forecast = scaler.inverse_transform( data_n_forecast )           # Undo 0 ---> 1 scaling
#   ------ ___ END ___: Predict n_forecast points from dataset end ------


#%% ------------------------- ___START___: Plot -------------------------
# NROWS = dataset.shape[0] + n_forecast                                   # NROWS = len(df) + n_forecast
DAY_DIFF = dataset.index[-1] - dataset.index[0]                         # Number of days between first and last entry in dataset
NROWS    = DAY_DIFF.days + n_forecast                                   # NROWS = DAY_DIFF.days + n_forecast
COLS = ['Date', 'Truth Set', 'Validation Set', 'Predictions']           # Columns we want in our final dataframe
final = pd.DataFrame( index     = range(0, NROWS+1),                    # Empty dataframe with columns
                      columns   = COLS )                                # ... specified above

final['Date'] = pd.date_range( start=pd.to_datetime( dataset.index[ 0] ),
                               end  =pd.to_datetime( dataset.index[-1] ) + pd.Timedelta(days=n_forecast) )
temp_df = dataset.reset_index()                                         # Set index [0, 1, ..., end] to facilitate looping

# TRAINING DATASET
for NDX in range( 0, SPLIT ):                                           # Add training dataset to final dataframe
    final.loc[ NDX, 'Truth Set' ] = temp_df.loc[ NDX, 'Close' ]         #   ...

# TRAINING DATASET 1 SET OF n_forecast PREDICTIONS FROM n_lookback
for NDX in range( SPLIT, SPLIT + n_forecast ):                          # Add 1 set of n_forecast predictions using
    final.loc[ NDX, 'Predictions' ] = train_n_forecast[ NDX-SPLIT, 0 ]  #   ... n_lookback of training dataset

# VALIDATION DATASET
for NDX in range( SPLIT-1, len(temp_df) ):                              # Add validation dataset to final dataframe
    final.loc[ NDX, 'Validation Set' ] = temp_df.loc[ NDX, 'Close' ]    #  SPLIT - 1; keeps continuity when plotting, in reality only need SPLIT

# VALIDATION DATASET n_forecast PREDICTIONS USING ENTIRE DATASET
for NDX in range( n_forecast, len( valid_n_forecast ) ):                # Add validation dataset predictions to
    final.loc[ (NDX + SPLIT), 'Predictions'] = valid_n_forecast[ NDX, 0 ]   # ... final dataframe
    
    # *** NOTE
    #   - n_forecast predictions are generated row-wise. In the previous step
    #     we only took the first entry in every row of generated predictions.
    #     Now that we are at the last row of n_forecast predictions, we would
    #     like to append the entire row to the final dataframe
    if( NDX == len( valid_n_forecast )-1 ):                             #   If we reached valid_n_forecast[ END, 0 ]
        for NDX_2 in range( NDX, NDX + n_forecast ):                    #       Add the remaining data points in that row of predictions
            final.loc[ NDX_2 + SPLIT, 'Predictions' ] = valid_n_forecast[ -1, NDX_2-NDX ]


# DATASET 1 SET OF n_forecast PREDICTIONS FROM 1 SET n_lookback DATA POINTS
for NDX in range( len( dataset ), len( dataset ) + n_forecast ):
    final.loc[ NDX, 'Predictions' ] = data_n_forecast[ (NDX - dataset.shape[0]), 0 ]


# Simplify dataframe
final.index = final['Date']                                             # Change index back to Date from 0 ---> end
x_min   = dt.datetime( 2020, 1, 1 )                                     # Define x-axis min limit
x_max   = pd.to_datetime( final['Date'][-1] + dt.timedelta(days=15) )   # Define x-axis max limit
final   = final.drop( 'Date', axis=1 )                                  # Drop/remove the Date header as we don't need it anymore

plt.figure( figsize=(10, 5) )
plt.ion()
plt.plot( final['Truth Set'     ] )
plt.plot( final['Validation Set'] )
plt.plot( final['Predictions'   ] )
plt.xlabel( 'Date', fontsize=8 )
plt.ylabel( 'Close Price USD ($)', fontsize=12 )
plt.xlim( [x_min, x_max] )
plt.legend( ['Truth Set', 'Validation Set', 'Predictions'], loc='lower right' )
plt.title( f'{ticker} price predictions' )
plt.show()

# Save figure
plt.savefig( f'{figure_dir}/{model_id}.pdf' )
# ------------------------- ___ END ___: Plot -------------------------

#@formatter:on