# Machine Learning with Python

Two models are provided:

1. Unidirectional long short-term memory (LSTM) model
2. Bidirectional  long short-term memory (LSTM) model

The models currently only support datasets with one feature. Plans to implement more feature support are in consideration.

## Usage

The models can be run with the default parameters. However, if desired, the following parameters can be changed:

#### Stock data parameters
- `ticker`: Stock\crypto ticker of interest [default = 'BTC-USD']
- `t_initial`: Start data of training dataset obtained from Yahoo.com [default = 2017-1-1]
- `t_final`: Start data of training dataset obtained from Yahoo.com [default = today]

#### LSTM model parameters
- `n_lookback`: Length of input  sequences (i.e. days in the past to use for generating predictions) [default = 5]
- `n_forecast`: Length of output sequences (i.e. days into the future to predict) [default = 3]
- `PRCNT`: Percentage of dataset to be used for training the model [default = 0.8 (i.e. 80%)]
- `BATCH_SIZE`:  Number of sequences that are trained together [default = 32]
- `N_EPOCHS`:  Number of iterations used for training [default = 200]
- `N_L`:  Number of layers in the neural network (i.e. 1 input layer + `N_L` layers) [default = 4]
